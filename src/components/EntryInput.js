import React, { Component, PropTypes } from 'react';
import {reduxForm} from 'redux-form';

export class EntryInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }
  }

  clear () {
    this.setState({
      value: ''
    });
  }

  handleChange(evt) {
    this.setState({
      value: evt.target.value
    });
  }

  render () {
    const {
      fields: { text },
      handleSubmit
    } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <label>Entry: </label>
      <input type="text" placeholder="New item..." {...text} />
      {' '}
      <button type="submit">Add</button>
      </form>
    )
  }
}

export default EntryInput = reduxForm({
  form: 'input',
  fields: ['text']})(EntryInput)
