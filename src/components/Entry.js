import React, { Component, PropTypes } from 'react';

export default class Entry extends Component {
  static propTypes = {
    increment: PropTypes.func.isRequired,
    decrement: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
  };

  constructor (props) {
    super(props);

    this.state = {
      counter: 0
    }
  }

  render() {
    const { increment, decrement, text } = this.props;
    return (
      <li>
        Clicked: {this.state.counter} times
        {' '}
        <button onClick={increment}>+</button>
        <span>{text}</span>
        <button onClick={decrement}>-</button>
      </li>
    );
  }
}
