import React, { Component, PropTypes } from 'react';
import Entry from './Entry';


export default class EntryTable extends Component {
  constructor (props) {
    super(props);

    this.state = {
      entries: [{id: 0,text: 'hello',counter:0}]
    }
  }

  render() {
    return (
      <div className='entry-table'>
    <ul>
      {this.state.entries.map((entry) => {
        <Entry key={entry.id} id={entry.id} counter={entry.counter}  text={entry.text}/>
      })}
    </ul>
    </div>
    );
  }
}

EntryTable.displayName = 'EntryTable';

export default EntryTable;
