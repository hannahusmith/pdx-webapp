import React, {Component, PropTypes} from 'react';

export default class Form extends Component {
  static propTypes = {
    children: PropTypes.node,
    values: PropTypes.object,
    update: PropTypes.func,
    reset: PropTypes.func,
    onSubmit: PropTypes.func
  }

  render () {
    return (
      <form>
        {this.props.children}
      </form>
    )
  }
}

Form.displayName = 'Form'
