import React, { Component, PropTypes } from 'react';
import {reduxForm} from 'redux-form';

export default class Input extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 'default'
    }
  }

    reset() {
      this.setState('')
    }

    handleChange = (evt) => {
      this.setState(evt.target.value)
    }

    handleSubmit = (data) => {
      this.props.onSubmit(this.target.value);

    }

    render () {
      const {fields: {text}, handleSubmit} = this.props;
      return (
        <form onSubmit={handleSubmit}>
          <div>
            <label>Entry Input:</label>
            <input type="text" placeholder="Entry" {...text}/>
          </div>
          <button type="submit">Submit</button>
        </form>
      );
    }
}

 Input = reduxForm({
  form: 'entryInput',
  fields: ['text']
})(Input)
export default Input;
