import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import Main from '../components/Main';
import * as entryActions from '../actions/entryActions';
import Input from '../components/Input';


export class AnotherPage extends Component {
  render() {
    return (
      <div>
        <h3>AnotherPage</h3>
        <Input handleSubmit={onSubmit}/>
        </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: bindActionCreators(entryActions.addEntry,dispatch)
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Input);
