import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import EntryTable from '../components/EntryTable';
import * as EntryActions from '../actions/entryActions';

function mapStateToProps(state) {
  return {
    entries: state.entries,
    counter: state.counter
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(EntryActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EntryTable);
