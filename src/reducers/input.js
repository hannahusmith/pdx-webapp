import {SUBMIT, RESET} from '../actions/inputActions'
import {ADD_ENTRY} from '../actions/entryActions'

export const input = (state, action) => {
  switch (action.type) {
    case SUBMIT:
      return dispatch => dispatch({
        type: ADD_ENTRY,
        text: action.value
      });
    default:
      return state;
  }
}
