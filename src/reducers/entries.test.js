import assert from 'assert';
import expect from 'expect';
import {NEW_ENTRY, INCREMENT_VOTES,DECREMENT_VOTES} from '../actions/entryActions';
import { entries, entry } from './entries';

describe('Test of single entry functionality', () => {
  it('should add single entry', function () {
    const action = {
      type: NEW_ENTRY,
      text: 'This is a brand-spankin-new entry'
    };
    const actual = entries(undefined,action);
    const expected = [{
      id:0,
      text: 'This is a brand-spankin-new entry',
      counter: 0
    }];
    assert.deepEqual(actual, expected);
  });
  it('should add entry and increment counter', function () {

    expect(
      entry({
        id:0,
        text: 'This is a brand-spankin-new entry',
        counter:0
    },{type: INCREMENT_VOTES, id: 0})
    ).toEqual({
      id: 0,
      text: 'This is a brand-spankin-new entry',
      counter: 1
    })
});
});

describe('Test of entry list functionality', () => {
  const oldstate = entries(undefined,{type: NEW_ENTRY, text: 'first'})
  const action = {type: NEW_ENTRY, text: 'new text'};
  it('should add two entries to list', function () {
    expect((entries(oldstate, action)).toEqual(
        [
          {id: 1, text: 'Number 1',counter: 1},
           {id:0,text:'Number 2',counter:0}
        ]
      )
    )
  })
});
