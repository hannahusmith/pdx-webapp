import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import entries from './entries';
import {reducer as form} from 'redux-form';

const rootReducer = combineReducers({
  entries,
  form,
  routing
});

export default rootReducer;
