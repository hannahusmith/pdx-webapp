import * as types from '../actions/entryActions';

let nextEntryId = 0;
export const entry = (state=[], action) => {
  switch (action.type) {
    case types.ADD_ENTRY:
      return {
        ...state,
        id: nextEntryId++,
        text: action.text,
        counter: 0
      }
    case types.INCREMENT_VOTES:
      if (action.id !== state.id) return state;
      return {
        ...state,
        counter: state.counter + 1
      }
    case types.DECREMENT_VOTES:
      if (action.id !== state.id) return state;
      return {
        ...state,
        counter: state.counter - 1
      }
    default:
      return state;
  }
}

export const entries = (state=[], action) => {
  switch (action.type) {
    case types.ADD_ENTRY:
      console.log('action', action);
      return [
        ...state,
        entry(undefined, action)
      ];
    default:
      return state;
  }
}
