import { createReducer } from 'redux-create-reducer';
import { INCREMENT_VOTES, DECREMENT_VOTES } from '../actions/counter';

const initialState = 0;

export default createReducer(initialState, {
  [INCREMENT_VOTES](state) {
    return state + 1;
  },
  [DECREMENT_VOTES](state) {
    return state - 1;
  }
});
