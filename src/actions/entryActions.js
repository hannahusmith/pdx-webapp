export const INCREMENT_VOTES = 'INCREMENT_VOTES';
export const DECREMENT_VOTES = 'DECREMENT_VOTES';
export const ADD_ENTRY = 'ADD_ENTRY';

export const increment = (id) => {
  return {
    type: INCREMENT_VOTES,
    id
  };
}

export const decrement = (id) => {
  return {
    type: DECREMENT_VOTES,
    id
  };
}

export const addEntry = (text) => {
  return {
    type: ADD_ENTRY,
    text
  };
}
