export const SUBMIT = 'SUBMIT';
export const RESET = 'RESET';
export const UPDATE_VALUE = 'UPDATE_VALUE';

import {ADD_ENTRY} from './entryActions';
export const submit = (text) => {
  return dispatch => dispatch({
    type: ADD_ENTRY,
    text
  });
}

export const reset = () => {
  return {
    type: RESET
  }
}
