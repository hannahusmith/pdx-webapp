export const FORM_SUBMIT = 'FORM_SUBMIT';
export const FORM_RESET = 'FORM_RESET';
export const FORM_UPDATE = 'FORM_UPDATE';

export const submit = (text) => {
  return dispatch => dispatch({
    type: ADD_ENTRY,
    text
  });
}

export const reset = () => {
  return dispatch => dispatch({
    type: FORM_RESET
  });
}

export const update = (name, value) => {
  return dispatch => dispatch({
    type: FORM_UPDATE,
    name, value
  });
}
